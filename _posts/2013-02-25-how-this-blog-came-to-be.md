---
layout: post
title: "How this blog came to be"
date: 2013-02-25 10:17
comments: true
categories: [octopress, blog, jekyll, gh-pages]
---

Since everyone have a blog, then I started one,

This blog is useing [Octopress](http://octopress.org/) which is baseed on [jekyll](http://jekyllrb.com/)

It’s pretty easy to get started. Now let’s set it up.

## Before You Begin

1 [Install Git](http://git-scm.com)
2 Install Ruby 1.9.3 using either [rbenv](http://octopress.org/docs/setup/rbenv) or [RVM](http://octopress.org/docs/setup/rvm/)
or [https://rvm.io/rvm/install/](https://rvm.io/rvm/install/)

If 'ruby --version' doesn’t say you’re using Ruby 1.9.3, revisit your [rbenv](http://octopress.org/docs/setup/rbenv) or [RVM](http://octopress.org/docs/setup/rvm/) installation

## Setup Octopress

{% highlight bash %}
git clone git://github.com/imathis/octopress.git octopress
cd octopress    # If you use RVM, You’ll be asked if you trust the .rvmrc file (say yes).
ruby --version  # Should report Ruby 1.9.3
{% endhighlight %}

### Next, install dependencies.

{% highlight bash %}
gem install bundler
rbenv rehash # If you use rbenv, rehash to be able to run the bundle command
rvm rehash # If you use rvm
bundle install
{% endhighlight %}

### Install the default Octopress theme.

{% highlight bash %}
rake install
{% endhighlight %}

## Deploying

### Github Pages

Hosting your blog with Github’s [Pages service](http://pages.github.com) is free and allows custon domains

[Check how to deploy to github pages](http://octopress.org/docs/deploying/github/)

## Configuring Octopress

Change your blog in `_config.yml` or check [Octopress configuring](http://octopress.org/docs/configuring/)
